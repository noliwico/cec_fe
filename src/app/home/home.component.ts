 import { Component, OnInit } from '@angular/core';
 import { CommService, GameStatus, GetGamesReply} from '../services/comm.service';
import { Router } from '@angular/router';
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  selectedGame: number;
  gameStatusList: GameStatus[];
  newGameDisplayName: string;
  newGameMaxPlayers: number;
  newGameId: string;
  newGameAdminId: string;

  constructor( private commService: CommService, private router: Router) { 
    this.selectedGame = -1;
  }
  
  ngOnInit() {
    console.log('home inited');
    this.refreshGameList();
    this.newGameMaxPlayers = 4;
  }

  changeSelectedGame(){

  }

  refreshGameList() {
    this.commService.callService('getGames', {}).subscribe( data=> {
      var getDataReply: GetGamesReply = Object(data);
      this.gameStatusList = getDataReply.games;
      if (this.gameStatusList.length > 0 ) {
        this.selectedGame = 0;
      } else {
        this.selectedGame = -1;
      }
    }, error=> {
      console.log('Communication error while getting games list...');
      console.log(error);
    });    
  }

  viewTheGame(){
    if ( this.selectedGame !== -1 ){
      this.router.navigate(['/showGame', {gameId: this.gameStatusList[this.selectedGame].gameId} ]);
    }
  }

  createNewGame() {
    this.commService.callService('createNewGame', {
      "displayName": this.newGameDisplayName,
      "maxPlayers": this.newGameMaxPlayers
    }).subscribe(data=>{
      var reply = Object(data);
      this.newGameId = reply.gameId;
      this.newGameAdminId = reply.adminId;
    }, error=> {
      console.log('Communication error while createing new game...');
      console.log(error);
    });
  }
}
