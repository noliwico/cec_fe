import { Injectable, Inject } from "@angular/core";
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse, HttpErrorResponse } from "@angular/common/http";
import { Observable } from "rxjs";
import { LoadingScreenService } from "./loading-screen.service";
import { finalize, tap, switchMap, filter, take } from "rxjs/operators";



@Injectable()
export class LoadingScreenInterceptor implements HttpInterceptor {

  activeRequests: number = 0;


  constructor(private loadingScreenService: LoadingScreenService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if ( request.urlWithParams.indexOf("getHistory",0) < 0) {
      if (this.activeRequests === 0) {
        this.loadingScreenService.startLoading();
      }
      this.activeRequests++;
    }

    return next.handle(request).pipe(
      finalize(() => {
        if ( request.urlWithParams.indexOf("getHistory",0) < 0) {
          this.activeRequests--;
          if (this.activeRequests === 0) {
            this.loadingScreenService.stopLoading();
          }
        }
      })
    )
    
  };

}