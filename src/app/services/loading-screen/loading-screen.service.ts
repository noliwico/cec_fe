import { Injectable } from '@angular/core';
import { Observable, Subject } from "rxjs";

@Injectable({ providedIn: 'root' })
export class LoadingScreenService {

  private loading: boolean = false;
  private loadingStatus = new Subject<boolean>();

  startLoading() {
    this.loading = true;
    this.loadingStatus.next(this.loading);
  }

  stopLoading() {
    this.loading = false;
    this.loadingStatus.next(this.loading);
  }

  getMessage(): Observable<boolean> {
    return this.loadingStatus.asObservable();
  }
}