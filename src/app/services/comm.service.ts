import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CommService {
  private serverUrl: string;

  constructor(private http: HttpClient) {
    this.serverUrl = 'http://localhost:8080/'; 
   }

  callService(serviceName: string, payload ) { 
    return this.http.post(this.serverUrl + serviceName, payload,  {});    
  }
}

export class CubeDetails {
  public playerId: string;
	public playerName: string;
	public posX: number;
	public posY: number;
	public healthPoint: number;
	public npc: boolean;
}

export class GameStatus {
	public gameId: string;
	public gameName: string;
  public maxPlayers: number;
	public cubes: CubeDetails[];
  public status: number;  
  public boardWidth: number;
  public boardHeight: number;
  public nextPlayerId: string;
}

export class BaseReply{
  public errorCode: number;
  public errorMessage: string;
}

export class GetGamesReply extends BaseReply{
  public games: GameStatus[];
}

export class GetStatusReply extends BaseReply{
	public gameStatus: GameStatus;
}

export class GetStatusRequest{
	public gameId: string;
	public playerId: string;
	public playerSecret: string;

  constructor( gameId: string ){
    this.gameId = gameId;
    this.playerId = '0';
    this.playerSecret = '0';
  }
}

export class GetHistoryRequest{
	public gameId: string;
  public from: number;

  constructor( gameId: string, from: number) {
    this.gameId = gameId;
    this.from = from;
  }
}

export class GetHistoryReply extends BaseReply {
  public historyItems: GameStatus[];
}