import { Component, OnInit, OnDestroy } from '@angular/core';
import { LoadingScreenService } from "../services/loading-screen/loading-screen.service";
import { Subscription } from "rxjs";

@Component({
  selector: 'app-loading-screen',
  templateUrl: './loading-screen.component.html',
  styleUrls: ['./loading-screen.component.scss']
})
export class LoadingScreenComponent implements OnInit {
  loading: boolean = false;
  loadingSubscription: Subscription;

  constructor(private loadingScreenService: LoadingScreenService) {
    this.loadingSubscription = this.loadingScreenService.getMessage().subscribe((value) => {
      this.loading = value;
    }); 
   }

  ngOnInit() {
   
  }

  ngOnDestroy() {
    this.loadingSubscription.unsubscribe();
  }
}
