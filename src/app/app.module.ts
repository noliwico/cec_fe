import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { CommService } from './services/comm.service';
import {HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LoadingScreenInterceptor } from './services/loading-screen/loading.interceptor';
import { HomeComponent } from './home/home.component';
import { LoadingScreenComponent } from './loading-screen/loading-screen.component';
import { ShowgameComponent } from './showgame/showgame.component';

import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoadingScreenComponent,
    ShowgameComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule      
  ],
  providers: [CommService, 
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoadingScreenInterceptor,
      multi:true
    }],
  bootstrap: [AppComponent]
})
export class AppModule { }
