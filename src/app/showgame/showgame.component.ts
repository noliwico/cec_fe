import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { GetGamesReply, GameStatus, CubeDetails, CommService, GetStatusRequest, GetStatusReply, GetHistoryRequest, GetHistoryReply} from '../services/comm.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-showgame',
  templateUrl: './showgame.component.html',
  styleUrls: ['./showgame.component.css']
})
export class ShowgameComponent implements OnInit {
  gameId:string;
  historyRequest: GetHistoryRequest;
  gameStatus: GameStatus;
  gameHistory: GameStatus[] = [];
  currentStep: number = 0;

  @ViewChild('canvas', { static: true })
  canvas: ElementRef<HTMLCanvasElement>; 
  private ctx: CanvasRenderingContext2D;

  timeLeft: number = 60;
  interval;
  scale:number=0;
  c;
  maxWidth: number =0;
  maxHeight: number = 0;

  constructor(private commService: CommService, private router: Router, private activeRoute: ActivatedRoute) {
   }

  ngOnInit() {
    this.setCanvasScale();
    this.gameId = this.activeRoute.snapshot.paramMap.get('gameId');
    this.historyRequest = new GetHistoryRequest(this.gameId, this.gameHistory.length);
    this.refreshData();
    console.log(this.gameId);
  }

  refreshData() {
    this.drawCanvas();
    if (this.currentStep < this.gameHistory.length-1 ) {
      this.currentStep++;
      return;
    }

    this.pauseTimer();
    this.historyRequest.from = this.gameHistory.length;
    this.commService.callService('getHistory', this.historyRequest).subscribe(data=>{
      var reply: GetHistoryReply = Object(data);
      for ( let currItem of reply.historyItems ) {
        this.gameHistory.push(currItem);
      }
      this.startTimer();
      this.drawCanvas();
    }, error=>{});    
  }

  restartTimer(){
    clearInterval(this.interval);
    this.currentStep = 0;
    this.startTimer();
  }

  startTimer() {
    this.interval = setInterval(() => {
      this.doTimerThing();
    },500)
  }

  pauseTimer() {
    clearInterval(this.interval);
  }

  doTimerThing() {
    this.refreshData();
  }

  setCanvasScale() {
    this.canvas.nativeElement.width = 800;
    this.canvas.nativeElement.height = 800;
    this.scale = this.canvas.nativeElement.width / 1000;
    if (this.canvas.nativeElement.width < this.canvas.nativeElement.height) {
      this.scale = this.canvas.nativeElement.height / 1000;
    }
    this.maxWidth = this.canvas.nativeElement.width / this.scale;
    this.maxHeight = this.canvas.nativeElement.height / this.scale;
    this.ctx = this.canvas.nativeElement.getContext('2d');
    this.ctx.scale(this.scale, this.scale);
  }

  drawCanvasFromStatus(){
    this.ctx.beginPath();
    this.ctx.fillStyle = 'grey';
    this.ctx.fillRect(0, 0, 800, 800);

    this.ctx.fill();
    this.ctx.stroke();
    for (let currCube of this.gameStatus.cubes) {
      this.ctx.beginPath();
      if ( currCube.npc === true ) {
        this.ctx.fillStyle = 'green';
      } else {
        this.ctx.fillStyle = 'red';
      }
      this.ctx.fillRect(currCube.posX * 100, currCube.posY * 100, 100, 100);
      this.ctx.fillStyle="white";
      this.ctx.font = "30px Verdana";

      if ( currCube.npc !== true ) {
        this.ctx.fillText(currCube.playerName, currCube.posX * 100 + 5, currCube.posY * 100 + 35, 90);
      }
      this.ctx.fillText(currCube.healthPoint.toString(), currCube.posX * 100 + 5, currCube.posY * 100 + 75, 90);
    }
  }

  drawCanvas(){
    this.ctx.beginPath();
    this.ctx.fillStyle = 'red';
    this.ctx.fillRect(0, 0, 800, 800);

    if ( this.gameHistory.length > 0 ) {
      this.ctx.fillStyle = 'grey';
      this.ctx.fillRect(0, 0, this.gameHistory[this.currentStep].boardWidth*100, this.gameHistory[this.currentStep].boardHeight*100);

      this.ctx.fill();
      this.ctx.stroke();
      for (let currCube of this.gameHistory[this.currentStep].cubes) {
        this.ctx.beginPath();
        if ( currCube.npc === true ) {
          this.ctx.fillStyle = 'green';
        } else {
          this.ctx.fillStyle = 'orange';
        }
        this.ctx.fillRect(currCube.posX * 100, currCube.posY * 100, 100, 100);
        this.ctx.fillStyle="white";
        this.ctx.font = "30px Verdana";

        if ( currCube.npc !== true ) {
          this.ctx.fillText(currCube.playerName, currCube.posX * 100 + 5, currCube.posY * 100 + 35, 90);
        }
        this.ctx.fillText(currCube.healthPoint.toString(), currCube.posX * 100 + 5, currCube.posY * 100 + 75, 90);
      }
    } 
  }


}
